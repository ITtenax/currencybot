/**
 * Created by valentyn on 1/13/17.
 */
"use strict";
let TelegramBot = require('node-telegram-bot-api'),
    fs = require('fs'),
    api = require('./GovBankApi');

class Bot{

    constructor(){
        this.telegram = new TelegramBot(this.getToken(), { polling: true })
    }

    getToken(){
        return JSON.parse(fs.readFileSync(Bot.getTokenPath, "utf-8", (err,token) => {
           if(err) console.log(err);
           return token;
        }))['token'];
    }

    static get getTokenPath(){
        return "./src/resources/token.json";
    }

    chat(){
        this.telegram.on("text", (message) => {
           api.getCurrency(message.text).then(result => {
               if(result != null) {
                   let template = `Name: ${result['txt']}\nRate: ${result['rate']}\nDate: ${result['exchangedate']}`;
                   this.telegram.sendMessage(message.chat.id, template);
               }
           });
        });
    }
}

module.exports = new Bot();