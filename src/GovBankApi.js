/**
 * Created by valentyn on 1/13/17.
 */
let request = require('request'),
    dateFormat = require('dateformat');

class GovBankApi{

    getCurrency(currency){
        return new Promise(done => {
            request(GovBankApi.getGovBankApi(), (error, response, body) => {
                if (!error && response.statusCode == 200) {
                    done(JSON.parse(body).find(currencyArr => {
                        if(currencyArr['cc'] === currency.toUpperCase()){
                            return true;
                        }
                    }));
                }
            })
        });
    }

    static getGovBankApi(){
        let date = new Date(Date.now());
        return GovBankApi.getGovBankApiByDate(dateFormat(date, 'yyyymmdd'));
    }
    static getGovBankApiByDate(date){
        return `https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?date=${date}&json`;
    }
}

module.exports = new GovBankApi();